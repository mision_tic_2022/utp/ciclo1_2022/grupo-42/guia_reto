'''
Ganancias:
si tiempo > 2
valor_interes = (cantidad * porcentaje_interes * tiempo) / 12
porcentaje_interes = 0.03
valor_total = valor_interes + cantidad

Pérdidas:
si tiempo <= 2
valor_perder = catidad * porcentaje_perder 
porcentaje_perder = 0.02
valor_total = cantidad - valor_perder
--------
parámetros de la función:
    *usuario: string
    *capital
    *tiempo
    ->Retornar una cadena de caracteres (String)
    --------------------------------
    *Estructura del String para las ganancias:
    Para el usuario {} La cantidad de dinero a recibir, según el monto inicial {} para un tiempo de {} meses es: {}

    *Estructura del String para las pérdidas:
    Para el usuario {} La cantidad de dinero a recibir, según el monto inicial {} para un tiempo de {} meses es: {}

'''

def CDT(usuario: str, capital: int, tiempo: int):
    mensaje = ''
    #Para el caso de las ganancias
    if tiempo > 2:
        porcentaje_interes = 0.03
        valor_interes = (capital * porcentaje_interes * tiempo) / 12
        #aquí calcular el valor total
        mensaje = "Genera ganancias"#f"Para el usuario {usuario} La cantidad de dinero a recibir, según el monto inicial {} para un tiempo de {tiempo} meses es: {}"
    else:#Para el caso de las pérdidas
        porcentaje_perder = 0.02
        valor_perder = capital * porcentaje_perder 
        #aquí calcular el valor total
        mensaje = "Genera pérdidas"#f"Para el usuario {usuario} La cantidad de dinero a recibir, según el monto inicial {} para un tiempo de {tiempo} meses es: {}"

    return mensaje


print( CDT('AB1012', 1000000, 1) )